# Tonic Design Code Challenge

This repo is my attempt at completing the Tonic Design Code Challenge. The project is written in Swift 2.2 (using Xcode 7.3). 

When the app launches, the code will attempt to fetch the people from the json file on the network. When that is returned, the names will be displayed in a list view. If loading that json file fails (network or parse error), there will be a single row in the table view the user can tap to retry the network call. 

Tapping the "Sort" button will toggle the sort order of the names displayed. 

Tapping the "+" button will show you a screen where you can enter a new person. When you enter text into the first/last name fields, a full name label will be updated. You can change the format of that label with a segmented control. 

Total time for completion was about 4 hours. 

## Description:

Create a screen that has three user configurable fields: [First Name], [Last Name], Name Display Format.

### Acceptance Criteria #1:

When a user enters content into the first name field, then the data should be displayed somewhere on the screen as it is entered

### Acceptance Criteria #2:

When a user enters content into the last name field, then the data should be displayed somewhere on the screen as it is entered

### Acceptance Criteria #3:

A user can choose from two Name Display Formats: 
	First Name First -> [First Name] [Last Name] John Doe
	Last Name First -> [Last Name], [First Name] Doe, John

### Acceptance Criteria #4:

When a user has not yet selected, or does not select, a Name Display Format and both the first and last name field have been filled in, then the name should appear as First Name First [First Name] [Last Name]

### Acceptance Criteria #5:

When a user selects a Name Display Format and both the first and last name field have been filled in, then the name should display as [First Name] [Last Name] when First Name First is selected and [Last Name], [First Name] when Last Name First is selected.

## Description:

Add a JSON file to the project via a network operation. Create a new screen that takes a collection of people in the JSON file and presents them in a sortable list. A user should be able to add a name to this list.

### Acceptance Criteria #6:

When given an external JSON file, the the app should consume the JSON file from the network endpoint [https://gist.github.com/joxenford/c49932a9ce74007e49b466cae8886fec](https://gist.github.com/joxenford/c49932a9ce74007e49b466cae8886fec)

## Creator

[Ryan Grier](http://github.com/rwgrier)  
[@rwgrier](https://twitter.com/rwgrier)
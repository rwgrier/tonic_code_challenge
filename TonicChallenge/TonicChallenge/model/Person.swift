//
//  Person.swift
//  TonicChallenge
//
//  Created by Grier, Ryan on 4/23/16.
//  Copyright © 2016 Ryan Grier. All rights reserved.
//

import Foundation

struct Person {
    let first: String?
    let last: String?
    
    var displayString: String {
        return "\(first ?? "") \(last ?? "")"
    }
}
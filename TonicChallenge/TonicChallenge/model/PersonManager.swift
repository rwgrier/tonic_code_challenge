//
//  PersonManager.swift
//  TonicChallenge
//
//  Created by Grier, Ryan on 4/23/16.
//  Copyright © 2016 Ryan Grier. All rights reserved.
//

import Foundation

enum SortOrder {
    case Ascending, Decending
}

enum LoadingState {
    case Initial, Loading, Loaded, Error
}

let endpoint: String = "https://gist.githubusercontent.com/joxenford/c49932a9ce74007e49b466cae8886fec/raw/8a999d3011cc000dec989538951c370c4bcfce5c/people.json"
let personAddedNotificationName: String = "com.ryangrier.personAddedNotificationName"

class PersonManager {
    var personCount: Int {
        return people.count
    }
    
    var sortOrder: SortOrder = .Ascending {
        didSet {
            resortPeople()
        }
    }
    
    private(set) var loadingState: LoadingState = .Initial
    private var people: [Person] = []
    
    func personAtIndex(index: Int) -> Person {
        return people[index]
    }
    
    func addPerson(firstName: String?, lastName: String?) {
        let person = Person(first: firstName, last: lastName)
        people.append(person)
        resortPeople()
        
        NSNotificationCenter.defaultCenter().postNotificationName(personAddedNotificationName, object: nil)
    }
    
    private func resortPeople() {
        let comparisonResult: NSComparisonResult = (sortOrder == .Ascending) ? .OrderedAscending : .OrderedDescending
        people.sortInPlace { return (($0.first) ?? "").compare((($1.first) ?? "")) == comparisonResult }
    }
}

// MARK: - Network operations

extension PersonManager {
    func fetchPeopleFromNetwork(completion: (success: Bool) -> ()) {
        loadingState = .Loading
        
        guard let jsonURL = NSURL(string: endpoint) else {
            self.loadingState = .Error
            completion(success: false)
            return
        }
        
        let dataTask: NSURLSessionDataTask = NSURLSession.sharedSession().dataTaskWithURL(jsonURL) { (data, response, error) in
            var success = false
            
            defer {
                self.loadingState = success ? .Loaded : .Error
                completion(success: success)
            }
            
            guard let data = data else { return }
            if let httpResponse = response as? NSHTTPURLResponse where httpResponse.statusCode == 200 {
                do {
                    guard let json = try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions(rawValue: 0)) as? [NSObject: AnyObject] else { return }
                    
                    self.people = self.peopleFrom(json)
                    self.resortPeople()
                    success = true
                } catch _ {
                    // In this simple case, eat it. We're not doing anything special with a JSON parse error.
                }
            }
        }
        
        dataTask.resume()
    }
    
    private func peopleFrom(json: [NSObject: AnyObject]?) -> [Person] {
        guard let json = json, peopleJSON = json["people"] as? [AnyObject] else { return [] }
        var people: [Person] = []
        
        for rawPersonJSON in peopleJSON {
            guard let personJSON = rawPersonJSON as? [NSObject: AnyObject] else { continue }
            if let person = personFrom(personJSON) {
                people.append(person)
            }
        }
        
        return people
    }
    
    private func personFrom(json: [NSObject: AnyObject]?) -> Person? {
        guard let json = json else { return nil }
        
        let first = json["first_name"] as? String
        let last = json["last_name"] as? String
        
        return Person(first: first, last: last)
    }
}
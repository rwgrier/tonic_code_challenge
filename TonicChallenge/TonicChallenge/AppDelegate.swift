//
//  AppDelegate.swift
//  TonicChallenge
//
//  Created by Grier, Ryan on 4/23/16.
//  Copyright © 2016 Ryan Grier. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}


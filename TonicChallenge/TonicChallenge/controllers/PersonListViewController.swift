//
//  PersonListViewController.swift
//  TonicChallenge
//
//  Created by Grier, Ryan on 4/23/16.
//  Copyright © 2016 Ryan Grier. All rights reserved.
//

import UIKit

class PersonListViewController: UITableViewController {
    @IBOutlet private weak var addButton: UIBarButtonItem!
    @IBOutlet private weak var sortButton: UIBarButtonItem!
    
    // Instead of having a singleton, let's use this approach
    private var personManager: PersonManager = PersonManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadPeopleFromNetwork()
        setupNotificationListeners()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    @IBAction func toggleSortOrder(sender: AnyObject?) {
        personManager.sortOrder = (personManager.sortOrder == .Ascending) ? .Decending : .Ascending
        refreshView()
    }
    
    func addPersonNotificationRecieved(notification: NSNotification) {
        tableView.reloadData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let navController = segue.destinationViewController as? UINavigationController, addPersonController = navController.topViewController as? AddPersonViewController {
            addPersonController.personManager = personManager
        }
    }
    
    private func loadPeopleFromNetwork() {
        // Kick off the network request
        personManager.fetchPeopleFromNetwork { [weak self] (success) in
            dispatch_async(dispatch_get_main_queue()) {
                self?.refreshView()
            }
        }
    }
    
    private func setupNotificationListeners() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(PersonListViewController.addPersonNotificationRecieved(_:)), name: personAddedNotificationName, object: nil)
    }
    
    private func refreshView() {
        tableView.reloadData()
        let enabled: Bool

        switch personManager.loadingState {
        case .Loaded:   enabled = true
        default:        enabled = false
        }
        
        addButton.enabled = enabled
        sortButton.enabled = enabled
    }
}

// MARK: - UITableViewDelegate

extension PersonListViewController {
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        defer { tableView.deselectRowAtIndexPath(indexPath, animated: true) }
        
        switch (personManager.loadingState, personManager.personCount) {
        case (.Error, _):
            loadPeopleFromNetwork()
            tableView.reloadData()
        case (.Loaded, 0):
            performSegueWithIdentifier("addPerson", sender: self)
        default:    break
        }
    }
}

// MARK: - UITableViewDataSource

extension PersonListViewController {
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Either show a placeholder row, or the actual people rows
        switch personManager.loadingState {
        case .Loaded:   return personManager.personCount
        default:        return 1
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(personManager.loadingState.cellIdentifier, forIndexPath: indexPath)
        
        if personManager.loadingState == .Loaded {
            let person = personManager.personAtIndex(indexPath.row)
            cell.textLabel?.text = person.displayString
        }
        
        return cell
    }
    
}

// MARK: - Loading State extension (cell identifiers)

extension LoadingState {
    var cellIdentifier: String {
        switch self {
        case .Loaded:   return "personCell"
        case .Error:    return "errorCell"
        default:        return "loadingCell"
        }
    }
}
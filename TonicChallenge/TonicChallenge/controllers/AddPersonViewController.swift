//
//  AddPersonViewController.swift
//  TonicChallenge
//
//  Created by Grier, Ryan on 4/23/16.
//  Copyright © 2016 Ryan Grier. All rights reserved.
//

import UIKit

enum NameFormat: Int {
    case FirstLast = 0, LastFirst
}

class AddPersonViewController: UIViewController {
    @IBOutlet private weak var firstNameField: UITextField!
    @IBOutlet private weak var lastNameField: UITextField!
    @IBOutlet private weak var fullNameLabel: UILabel!
    @IBOutlet private weak var formatControl: UISegmentedControl!
    
    var personManager: PersonManager?
    private var format: NameFormat = .FirstLast {
        didSet {
            refreshFullNameField(firstNameField.text, last: lastNameField.text)
        }
    }
    
    @IBAction func savePerson(sender: AnyObject?) {
        // Save the person
        personManager?.addPerson(firstNameField.text, lastName: lastNameField.text)
        closeModal()
    }
    
    @IBAction func cancel(sender: AnyObject?) {
        closeModal()
    }
    
    @IBAction func formatChanged(sender: UISegmentedControl) {
        format = NameFormat(rawValue: sender.selectedSegmentIndex) ?? .FirstLast
    }
    
    private func closeModal() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    private func refreshFullNameField(first: String?, last: String?) {
        switch format {
        case .FirstLast:
            fullNameLabel.text = "\(first ?? "") \(last ?? "")"
        default:
            let seperator: String = (last?.characters.count ?? 0) > 0 ? "," : ""
            fullNameLabel.text = "\(last ?? "")\(seperator) \(first ?? "")"
        }
    }
}

extension AddPersonViewController: UITextFieldDelegate {
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        var first: String?
        var last: String?
        
        if textField == firstNameField {
            let currentString = firstNameField.text ?? ""
            first = (currentString as NSString).stringByReplacingCharactersInRange(range, withString: string)
            last = lastNameField.text
        } else {
            let currentString = lastNameField.text ?? ""
            first = firstNameField.text
            last = (currentString as NSString).stringByReplacingCharactersInRange(range, withString: string)
        }
        
        refreshFullNameField(first, last: last)
        return true
    }
}

